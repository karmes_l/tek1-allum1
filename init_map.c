/*
** init_map.c for  in /home/karmes_l/Projets/Prog_Elem/Allum1/v1/allum1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Feb  2 11:05:49 2015 lionel karmes
** Last update Mon Feb 16 12:10:12 2015 lionel karmes
*/

#include "my.h"

void	print_data_map(int i, int len_allum_y)
{
  my_putnbr(i + 1);
  if (i + 1 < 10)
    my_putchar(' ');
  my_putstr("      ");
  my_putnbr(len_allum_y);
  if (len_allum_y / 10 < 1)
    my_putchar(' ');
  my_putstr("         ");
}

void	print_map(int *map, int len)
{
  int	i;
  int	x;

  i = 0;
  my_putstr("Rangée  Allumette\n");
  while (i < len)
    {
      print_data_map(i, map[i]);
      x = 0;
      while (x++ < len - i - 1)
	my_putchar(' ');
      x = 0;
      while (x++ < map[i])
	my_putchar('|');
      my_putchar('\n');
      i++;
    }
}

void	load_map(int **map, int y)
{
  int	i;
  int	x;

  i = 0;
  x = 1;
  if ((*map = pmalloc(sizeof(int) * y)) == NULL)
    exit(0);
  while (i < y)
    {
      (*map)[i] = x;
      x += 2;
      i++;
    }
}

void	init_map(t_map *map, int player_total)
{
  int	y;

  y = prompt_subject("Choisissez le nombre de rangée :\n", "rangée");
  if (y > 35 || y <= player_total)
    {
      my_putstrerror("Le nombre de rangée maximal ne doit pas dépasser 35 ");
      my_putstrerror("et ne doit pas être en dessous de ");
      my_putnbr(player_total + 1);
      my_putcharerror('\n');
      init_map(map, player_total);
    }
  else
    {
      map->len_allum = y * (y + 1)  - y;
      map->len = y;
      load_map(&(map->map), map->len);
    }
}
