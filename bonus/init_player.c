/*
** allum1.c for  in /home/karmes_l/Projets/Prog_Elem/Allum1/v1/allum1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Feb  2 10:52:49 2015 lionel karmes
** Last update Mon Feb 16 12:07:38 2015 lionel karmes
*/

#include "my.h"


int		choice_turn_man_valid1(t_player *player, int turn)
{
  if (turn < 1 || turn > player->total)
    {
      my_putstrerror("Ce nombre de tour n'existe pas");
      my_putstrerror("(il n'y a qu'un total de ");
      my_putnbr(player->total);
      my_putstrerror(" joueurs).\n");
      my_putstrerror("Vous êtes revenu à la selection des tours de ");
      my_putstrerror("joueur humain\n");
      init_int(&(player->turn_man), player->man, 0);
      choice_turn_man(player);
      return (0);
    }
  return (1);
}

void		choice_turn_man(t_player *player)
{
  int		i;
  int		turn;

  i = 0;
  while (i < player->man)
    {
      my_putstr("Le ");
      my_putnbr(i + 1);
      (i == 0) ? my_putstr("er") : my_putstr("ème");
      turn = prompt_subject(" joueur humain doit jouer à quel tour ?\n", "tour");
      if (!choice_turn_man_valid1(player, turn))
	break;
      else if (find_element_int(player->turn_man, player->man, turn))
	my_putstrerror("Un joueur humain occupe déjà ce tour\n");
      else
	{
	  player->turn_man[i] = turn;
	  i++;
	}
    }
}

void		init_player(t_player *player)
{
  my_putstr("Combien de joueur humain voulez-vous ajouter ?\n");
  player->man = prompt_subject("", "joueur humain");
  player->ia = prompt_subject("Combien d'IA voulez-vous ajouter ?\n", "IA");
  player->total = player->man + player->ia;
  if (player->total < 2 || player->total > 6)
    {
      my_putstrerror("Il doit y avoir plus de 2 joueurs et moins de 7");
      my_putstrerror(" joueurs (IA inclus)\n");
      init_player(player);
    }
  else
    {
      if ((player->turn_man = pmalloc(sizeof(int) * player->man)) == NULL)
	exit(0);
      init_int(&(player->turn_man), player->man, 0);
      choice_turn_man(player);
    }
}
