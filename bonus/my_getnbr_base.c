/*
** allum1.c for  in /home/karmes_l/Projets/Prog_Elem/Allum1/v1/allum1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Feb  2 10:52:49 2015 lionel karmes
** Last update Mon Feb  9 11:48:13 2015 lionel karmes
*/

#include "my.h"

int	my_getnbr_base(int base_10, int base_to)
{
  int	reste;
  int	i;

  i = 0;
  reste = 0;
  while (base_10 > 0)
    {
      reste += base_10 % base_to * pow_10(i);
      base_10 /= base_to;
      i++;
    }
  return (reste);
}
