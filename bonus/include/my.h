/*
** my.h for  in /home/karmes_l/test/tmp_Piscine_C_J09
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Thu Oct  9 15:22:30 2014 lionel karmes
** Last update Mon Feb 16 15:44:41 2015 lionel karmes
*/

#ifndef MY_H_
# define MY_H_

#include <stdlib.h>
#include <unistd.h>
#include "get_next_line.h"
#include "allum1.h"

# define MAX(v1, v2)	((v1) > (v2)) ? (v1) : (v2)

char		*convert_base(char *, char *, char *);
int		count_num(long nb);
int		my_charisnum(char);
void		my_putchar(char);
void		my_putchar0(char);
void		my_putcharerror(char);
void		my_putnbr(long);
void		my_swap(int *, int *);
void		my_putstr(char *);
void		my_putstrerror(char *);
int		my_strlen(char *);
int		my_getnbr(char *);
void		my_sort_int_tab(int *, int);
char		*my_strcpy(char *, char *);
char		*my_strncpy(char *, char *, int);
char		*my_revstr(char *);
char		*my_strstr(char *, char *);
char		*my_strdup(char *);
int		my_strcmp(char *, char *);
int		my_strncmp(char *, char *, int n);
char		*my_strupcase(char *);
char		*my_strlowcase(char *);
char		*mystrcapitalize(char *);
int		my_str_isalpha(char *);
int		my_str_isnum(char *);
int		mu_str_islower(char *);
int		my_str_isupper(char *);
int		my_str_isprintable(char *);
char		*my_strcat(char *, char *);
char		*my_strncat(char *, char *, int);
int		my_strlcat(char *, char *, int);
int		my_str_isnum2(char *);
unsigned long	pow_10(int);
int		power(unsigned long, unsigned long);
int		my_putchar2(int);
char		**my_str_to_wordtab(char *, char);
void		*pmalloc(int);
void		allum1();
char		*prompt(char *);
int		prompt_subject(char *, char *);
void		init_map(t_map *, int);
void		print_map(t_map *);
void		take_allum(t_map *, int);
void		game_turn_player(t_map *);
void		game_turn_ia(t_map *);
void		take_allum_next(t_map *, int, int);
void		stat_turn_player(char *, int);
int		find_element_int(int *, int, int);
void		init_player(t_player *);
void		init_int(int **, int, int);
void		choice_turn_man(t_player *);
int		choice_turn_man_valid1(t_player *, int);
int		my_getnbr_base(int, int);
int		range_map(t_map *);
int		range_allum_1(t_map *);
int		nbr_num_pair(int);
int		somme_bin_map(t_map *);
int		range_live(t_map *);
void		figure_map(t_map *, int);
void		load_map(t_map *);
void		load_allumette(t_map *);
void		print_map_allumette(t_map *);
void		print_data_map(int, int);

#endif /* !MY_H_ */
