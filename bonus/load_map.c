/*
** init_map.c for  in /home/karmes_l/Projets/Prog_Elem/Allum1/v1/allum1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Feb  2 11:05:49 2015 lionel karmes
** Last update Mon Feb 16 16:08:42 2015 lionel karmes
*/

#include "my.h"

void	load_pyramide(t_map *map)
{
  int	i;
  int	x;

  i = 0;
  x = 1;
  map->len_allum = map->len * (map->len + 1) - map->len;
  if ((map->map = pmalloc(sizeof(int) * map->len)) == NULL)
    exit(0);
  while (i < map->len)
    {
      map->map[i] = x;
      x += 2;
      i++;
    }
}

void	load_carre(t_map *map)
{
  int	i;

  i = 0;
  map->len_allum = map->len * map->len;
  if ((map->map = pmalloc(sizeof(int) * map->len)) == NULL)
    exit(0);
  while (i < map->len)
    {
      map->map[i] = map->len;
      i++;
    }
}

void	load_allumette(t_map *map)
{
  map->len_allum = 84;
  if ((map->map = pmalloc(sizeof(int) * map->len)) == NULL)
    exit(0);
  map->map[0] = 24;
  map->map[1] = 22;
  map->map[2] = 14;
  map->map[3] = 25;
}

void	load_map(t_map *map)
{
  void	(*ptr[2])(t_map *);

  ptr[0] = &load_pyramide;
  ptr[1] = &load_carre;
  ptr[map->figure](map);
}

