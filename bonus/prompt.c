/*
** prompt.c for  in /home/karmes_l/Projets/Prog_Elem/Allum1/v1/allum1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Feb  2 10:56:13 2015 lionel karmes
** Last update Tue Feb 17 17:20:40 2015 lionel karmes
*/

#include "my.h"

char	*prompt(char *message)
{
  char	*buff;

  my_putstr(message);
  buff = get_next_line(0);
  if (buff == NULL)
    exit(0);
  return (buff);
}

int	prompt_subject(char *message, char *subject)
{
  char	*buff;
  int	y;

  buff = prompt(message);
  while (!my_str_isnum(buff))
    {
      free(buff);
      my_putstrerror("Le ");
      my_putstrerror(subject);
      my_putstrerror(" doit être composé QUE de chiffre ");
      my_putstrerror("(donc il ne peut pas y avoir aussi un ");
      my_putstrerror(subject);
      my_putstrerror(" negative)");
      my_putcharerror('\n');
      buff = prompt(message);
    }
  y = my_getnbr(buff);
  free(buff);
  return (y);
}
