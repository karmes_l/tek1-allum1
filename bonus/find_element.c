/*
** find_element_int.c for  in /home/karmes_l/Projets/Prog_Elem/Allum1/v1/allum1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Tue Feb  3 12:05:29 2015 lionel karmes
** Last update Tue Feb  3 12:40:20 2015 lionel karmes
*/

int	find_element_int(int *tab, int size, int element)
{
  int	i;

  i = 0;
  while (i < size)
    {
      if (tab[i] == element)
	return (1);
      i++;
    }
  return (0);
}

void	init_int(int **tab, int size, int value)
{
  int	i;

  i = 0;
  while (i < size)
    {
      (*tab)[i] = value;
      i++;
    }
}
