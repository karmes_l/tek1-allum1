/*
** allum1.c for  in /home/karmes_l/Projets/Prog_Elem/Allum1/v1/allum1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Feb  2 10:52:49 2015 lionel karmes
** Last update Mon Feb 16 14:01:04 2015 lionel karmes
*/

#include "my.h"

void	take_allum_next(t_map *map, int col, int x)
{
    map->len_allum -= x;
    map->map[col] -= x;
}

void	take_allum_valid(t_map *map, int col)
{
  int	x;

  my_putstr("Combien d'allumette(s) voulez-vous enlever ? ");
  my_putstr("(100 = toutes; 0 = rechoisir la rangée)\n");
  x = prompt_subject("", "nombre de rangée");
  if (x == 0)
    game_turn_player(map);
  else if (x == 100 && map->len_allum - map->map[col] >= 1)
    take_allum_next(map, col, map->map[col]);
  else if (map->map[col] - x < 0 || map->len_allum - x < 1)
    {
      my_putstrerror("Impossible d'enlever ce nombre d'allumette\n");
      take_allum_valid(map, col);
    }
  else
    take_allum_next(map, col, x);
}

void	game_turn_player(t_map *map)
{
  int	y;

  print_map(map);
  my_putstr("Sur quelle rangée voulez-vous enlever vos allumette(s) ?\n");
  y = prompt_subject("", "nombre de rangée");
  y--;
  if (y < 0 || y >= map->len)
    {
      my_putstrerror("Cette rangée n'existe pas\n");
      game_turn_player(map);
    }
  else if (map->map[y] < 1)
    {
      my_putstrerror("Il n'y a plus d'allumette sur cette rangée\n");
      game_turn_player(map);
    }
  else
    take_allum_valid(map, y);
}

void	stat_turn_player(char *str, int player)
{
  my_putstr(str);
  my_putnbr(player);
  my_putchar('\n');
}
