/*
** allum1.c for  in /home/karmes_l/Projets/Prog_Elem/Allum1/v1/allum1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Feb  2 10:52:49 2015 lionel karmes
** Last update Mon Feb  9 15:43:14 2015 lionel karmes
*/

#include "my.h"

int	nbr_num_pair(int nbr)
{
  int	nbr_num;

  nbr_num = count_num(nbr);
  while (nbr_num > 0)
    {
      if ((nbr / pow_10(nbr_num - 1)) % 2 != 0)
	return (0);
      nbr -= nbr / pow_10(nbr_num - 1) * pow_10(nbr_num - 1);
      nbr_num--;
    }
  return (1);
}

int	somme_bin_map(t_map *map)
{
  int	y;
  int	somme_bin;

  y = 0;
  somme_bin = 0;
  while (y < map->len)
    {
      somme_bin += my_getnbr_base(map->map[y], 2);
      y++;
    }
  return (somme_bin);
}

int	range_live(t_map *map)
{
  int	i;
  int	range;

  range = 0;
  i = 0;
  while (i < map->len)
    {
      if (map->map[i] > 0)
	range++;
      i++;
    }
  return (range);
}

int	range_allum_1(t_map *map)
{
  int	i;
  int	allum_1;

  i = 0;
  allum_1 = 0;
  while (i < map->len)
    {
      if (map->map[i] == 1)
	allum_1++;
      i++;
    }
  return (allum_1);
}

int	range_map(t_map *map)
{
  int	i;
  int	range;

  i = 0; 
  range = 0;
  while (i < map->len)
    {
      if (map->map[i] > 0)
	range++;
      i++;
    }
  return (range);
}
