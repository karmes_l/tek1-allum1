/*
** allum1.c for  in /home/karmes_l/Projets/Prog_Elem/Allum1/v1/allum1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Feb  2 10:52:49 2015 lionel karmes
** Last update Mon Feb 16 13:59:54 2015 lionel karmes
*/

#include "my.h"

void		free_all(t_map *map, t_player *player)
{
  free(map->map);
  free(player->turn_man);
}


void		allum1_game()
{
  t_map		map;
  t_player	player;
  int		turn_player;
  
  init_player(&player);
  figure_map(&map, player.total);
  turn_player = 1;
  while (map.len_allum > 1)
    {
      stat_turn_player("\n\nTour du joueur : ", turn_player);
      if (find_element_int(player.turn_man, player.man, turn_player) ||
	  player.ia == 0)
	game_turn_player(&map);
      else
	game_turn_ia(&map);
      turn_player++;
      if (turn_player == player.total + 1)
	turn_player = 1;
    }
  stat_turn_player("\n\nLe perdant est joueur : ", turn_player);
  print_map(&map);
  free_all(&map, &player);
}

void		replay()
{
  char		*buff;

  my_putstr("Voulez-vous rejouer");
  
  buff = prompt(" (\"oui\" pour rejouer; \"non\" pour ne pas rejouer) ?\n");
  if (!my_strcmp("oui", buff))
    allum1();
  else if (my_strcmp("non", buff) != 0)
    {
      my_putstrerror("Veuillez rentrer \"oui\" pour rejouer ou \"non\"");
      my_putstrerror(" pour ne pas rejouer\n");
      replay();
    }
  free(buff);
}

void		allum1()
{
  allum1_game();
  replay();
}
