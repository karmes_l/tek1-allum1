/*
** init_map.c for  in /home/karmes_l/Projets/Prog_Elem/Allum1/v1/allum1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Feb  2 11:05:49 2015 lionel karmes
** Last update Mon Feb 16 14:39:45 2015 lionel karmes
*/

#include "my.h"

void	figure_map(t_map *map, int player_total)
{
  int	figure;

  my_putstr("Choisissez la figure d'allumettes\n");
  my_putstr("1. Pyramide\n");
  my_putstr("2. Carre\n");
  my_putstr("3. Allumette\n");
  figure = prompt_subject("", "numéro de figure d'allumettes");
  if (figure < 1 || figure > 3)
    {
      my_putstrerror("Le numéro de la figure est invalide\n");
      figure_map(map, player_total);
    }
  else
    {
      figure--;
      map->figure = figure;
      if (figure == ALLUMETTE)
	{
	  map->len = 4;
	  load_allumette(map);
	}
      else
	init_map(map, player_total);
    }
}

  void	init_map(t_map *map, int player_total)
{
  int	y;
  
  y = prompt_subject("Choisissez le nombre de rangée :\n", "nombre de rangée");
  if (y > 35 || y <= player_total)
    {
      my_putstrerror("Le nombre de rangée maximal ne doit pas dépasser 35 ");
      my_putstrerror("et ne doit pas être en dessous de ");
      my_putnbr(player_total + 1);
      my_putcharerror('\n');
      init_map(map, player_total);
    }
  else
    {
      map->len_allum = y * (y + 1) - y;
      map->len = y;
      load_map(map);
    }
}
