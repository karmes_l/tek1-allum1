/*
** print_map_allumette.c for  in /home/karmes_l/Projets/Prog_Elem/Allum1/v1/allum1/bonus
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Feb 16 15:19:50 2015 lionel karmes
** Last update Mon Feb 16 16:12:33 2015 lionel karmes
*/

#include "my.h"

void	buff_map_allumette_1(char **buff)
{
  *buff = my_strdup(" /\\   |   |   |   | |\\  | |\\  |  __ ____ ____  __");
  if (*buff == NULL)
    exit(0);
}

void	buff_map_allumette_2(char **buff)
{
  *buff = my_strdup("/__\\  |   |   |   | | \\/| | \\/| |_   |    |   |_");
  if (*buff == NULL)
    exit(0);
}

void	buff_map_allumette_3(char **buff)
{
  *buff = my_strdup("|   | |   |   |   | |   | |   | |    |    |   |");
  if (*buff == NULL)
    exit(0);
}

void	buff_map_allumette_4(char **buff)
{
  *buff = my_strdup("|   | |__ |__ |___| |   | |   | |__  |    |   |__");
  if (*buff == NULL)
    exit(0);
}

void	print_map_allumette(t_map *map)
{
  int	y;
  int	x;
  char	*buff;
  int	allum;
  void	(*ptr[4])(char **);

  y = 0;
  ptr[0] = &buff_map_allumette_1;
  ptr[1] = &buff_map_allumette_2;
  ptr[2] = &buff_map_allumette_3;
  ptr[3] = &buff_map_allumette_4;
  while (y < map->len)
    {
      print_data_map(y, map->map[y]);
      allum = 0;
      ptr[y](&buff);
      x = 0;
      while (buff[x] != '\0' && allum < map->map[y])
      	{
      	  if (buff[x] != ' ')
      	    allum++;
      	  my_putchar(buff[x++]);
      	}
      y++;
      my_putchar('\n');
      free(buff);
    }
}
