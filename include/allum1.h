/*
** map.h for  in /home/karmes_l/Projets/Prog_Elem/Allum1/v1/allum1/include
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Feb  2 11:34:50 2015 lionel karmes
** Last update Wed Feb 11 10:47:19 2015 lionel karmes
*/

#ifndef ALLUM1_H_
# define ALLUM1_H_

typedef struct	s_map
{
  int		*map;
  int		len;
  int		len_allum;
}		t_map;

typedef struct	s_player
{
  int		ia;
  int		*turn_man;
  int		man;
  int		total;
}		t_player;

#endif /* !MINISHELL_H_ */
