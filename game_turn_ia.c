/*
** allum1.c for  in /home/karmes_l/Projets/Prog_Elem/Allum1/v1/allum1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Feb  2 10:52:49 2015 lionel karmes
** Last update Mon Feb  9 16:22:18 2015 lionel karmes
*/

#include "my.h"

void	add_allum(t_map *map, int col, int x)
{
  map->len_allum += x;
  map->map[col] += x;
}

void	take_allum_ia_1(t_map *map)
{
  int	i;
  int	tmp;
  int	allum;

  i = 0;
  tmp = 0;
  allum = 0;
  while (i < map->len)
    {
      if (allum < map->map[i])
	{
	  allum = map->map[i];
	  tmp = i;
	}
      i++;
    }
  if ((range_map(map) == 1 || range_allum_1(map) % 2 == 0)
      && map->map[tmp] > 1)
    take_allum_next(map, tmp, map->map[tmp] - 1);
  else
    take_allum_next(map, tmp, map->map[tmp]);
}

void	take_allum_ia_2(t_map *map)
{
  int	somme_bin;
  int	y;
  int	x;

  somme_bin = 1;
  y = 0;
  x = 0;
  while (!nbr_num_pair(somme_bin) && y < map->len)
    {
      add_allum(map, y, x);
      x++;
      while (y < map->len && map->map[y] - x < 0)
	{
	  y++;
	  x = 1;
	}
      if (y < map->len)
	take_allum_next(map, y, x);
      somme_bin = somme_bin_map(map);
    }
  if (!nbr_num_pair(somme_bin))
    take_allum_ia_1(map);
}

void	game_turn_ia(t_map *map)
{
  int	somme_bin;

  print_map(map->map, map->len);
  somme_bin = somme_bin_map(map);
  if (nbr_num_pair(somme_bin) || range_map(map) == 1 ||
      (range_allum_1(map) + 1 >= range_live(map))) /* ia perd || ia gagne */
    take_allum_ia_1(map);
  else
    take_allum_ia_2(map); /* ia gagne */
}
