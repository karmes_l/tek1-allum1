##
## Makefile for  in /home/karmes_l/Projets/Maths
## 
## Made by lionel karmes
## Login   <karmes_l@epitech.net>
## 
## Started on  Mon Nov  3 16:51:51 2014 lionel karmes
## Last update Mon Feb 16 12:33:05 2015 lionel karmes
##

CC	= gcc

RM	= rm -f

CFLAGS	+= -Wextra -Wall -Werror
CFLAGS	+= -ansi -pedantic
CFLAGS	+= -I./include/ 

LDFLAGS	=

NAME	= allum1

LIB	= lib/

LIBNAME	= $(addprefix $(LIB), libmy.a)

SRCS	= main.c \
	allum1.c \
	get_next_line.c \
	prompt.c \
	init_map.c \
	game_turn_ia.c \
	game_turn_player.c \
	init_player.c \
	find_element.c \
	my_getnbr_base.c \
	map_allum.c

SRCS2	= count_num.c \
	  convert_base.c \
	  my_charisalpha.c \
	  my_charisnum.c \
	  my_getnbr.c \
	  my_putchar.c \
	  my_putcharerror.c \
	  my_putnbr.c \
	  my_putstr.c \
	  my_putstrerror.c \
	  my_revstr.c \
	  my_show_wordtab.c \
	  my_strcapitalize.c \
	  my_strcat.c \
	  my_strcmp.c \
	  my_strcpy.c \
	  my_strdup.c \
	  my_str_isalpha.c \
	  my_str_islower.c \
	  my_str_isnum.c \
	  my_str_isprintable.c \
	  my_str_isupper.c \
	  my_str_to_wordtab.c \
	  my_strlen.c \
	  my_strlowcase.c \
	  my_strncat.c \
	  my_strncmp.c \
	  my_strncpy.c \
	  my_strnum_to_wordtab.c \
	  my_strstr.c \
	  my_strupcase.c \
	  my_swap.c \
	  pmalloc.c \
	  pow_10.c \
	  power.c

SRCSLIB	= $(addprefix $(LIB), $(SRCS2))

OBJS	= $(SRCS:.c=.o)

OBJSLIB	= $(SRCSLIB:.c=.o)


all: $(LIBNAME) $(NAME)

$(NAME): $(OBJS)
	$(CC) $(OBJS) -o $(NAME) $(CFLAGS) $(LDFLAGS) -L./lib/ -lmy

$(LIBNAME): $(OBJSLIB)
	make -C $(LIB)
clean:
	$(RM) $(OBJS)
	make clean -C $(LIB)

fclean: clean
	$(RM) $(NAME)
	make fclean -C $(LIB)

re: fclean all

.PHONY: all clean fclean re
