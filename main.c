/*
** main.c for  in /home/karmes_l/Projets/Prog_Elem/Allum1/v1/allum1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Feb  2 10:50:49 2015 lionel karmes
** Last update Mon Feb  2 10:55:16 2015 lionel karmes
*/

#include "my.h"

int	main(int ac, char **av)
{
  (void) av;
  if (ac > 1)
    my_putstrerror("Not argument required.\n");
  else
    allum1();
  return (1);
}
